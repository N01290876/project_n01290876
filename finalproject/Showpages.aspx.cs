﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace finalproject
{
    public partial class Showpages : System.Web.UI.Page
    {

        private string query = "SELECT pageid , pagetitle as Title ,pagecontent as content from pages";


        protected DataView Page_Manual_Bind(SqlDataSource src)
        {


            DataTable tbl;
            DataView view;
            tbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            DataColumn col = new DataColumn();
            col.ColumnName = "Edit";
            tbl.Columns.Add(col);
            foreach (DataRow row in tbl.Rows)
            {
                row[col] = "<a href=\"Edit.aspx?pageid=" + row["pageid"] + "\">Edit</a>";
                row["Title"] = "<a href=\"Edit.aspx?pageid=" + row["pageid"] + "\">" + row["Title"] + "</a>" + "";



            }
            tbl.Columns.Remove("pageid");
            view = tbl.DefaultView;

            return view;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            page_list.SelectCommand = query;
           display.DataSource = Page_Manual_Bind(page_list);
            display.DataBind();


        }

        

    }
}