﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace finalproject
{
    public partial class SiteMaster : MasterPage
    {
        private string query = "Select pageid , pagetitle as Title from pages";


        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!IsPostBack)
            {
                page_pick.SelectCommand = query;
                PagePick_Manual_Bind(page_pick);

            }

        }


        protected void PagePick_Manual_Bind(SqlDataSource src)
        {


            DataTable mytbl;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            
            foreach (DataRow row in mytbl.Rows)
            {

                row["Title"] = "<li><a href=\"Edit.aspx?pageid=" + row["pageid"] + "\">" + row["Title"] + "</a></li>";

                list.InnerHtml += row["Title"].ToString();

            }
                


        }
    }
}