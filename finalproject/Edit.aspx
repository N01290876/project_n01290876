﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="finalproject.Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    
    <h3 runat="server" id="pageinfo">Edit Page</h3>
     <div >
        <label>Page title</label>
        <asp:TextBox ID="pageTitle" runat="server" ></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="pageTitle" ErrorMessage="Enter a Page Title">
        </asp:RequiredFieldValidator>
           
    </div>
    
  <div>
     <label>Page content</label>
    </div>
     <asp:TextBox ID="Content" TextMode="multiline" Columns="30" Rows="10" runat="server" ></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="Content" ErrorMessage="Enter a Page Content">
        </asp:RequiredFieldValidator>

    <asp:SqlDataSource runat="server" id="page_show" ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" id="editpage" ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server"  id="delpage" ConnectionString="<%$ ConnectionStrings:school_sql_con %>">
    </asp:SqlDataSource>
    
 <div>
    <asp:Button Text="Edit" runat="server" OnClick="edit"/>
     <asp:Button runat="server" id="del_btn"
        OnClick="delete" OnClientClick="if(!confirm('Confirm to delete Page?')) return false;"
        Text="Delete Page" />
   </div>
    <div id="result" runat="server"></div>

</asp:Content>
