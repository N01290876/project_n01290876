﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace finalproject
{
    public partial class Edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            DataRowView pagerow = getInfo(pageid);
            if (pagerow == null)
            {
                pageinfo.InnerHtml = "No Page Found.";
                return;
            }


            pageTitle.Text = pagerow["pagetitle"].ToString();

            Content.Text = pagerow["pagecontent"].ToString();


        }

        protected DataRowView getInfo(int id)
        {
            string query = "select * from pages where pageid=" + pageid.ToString();
            page_show.SelectCommand = query;
            DataView pageview = (DataView)page_show.Select(DataSourceSelectArguments.Empty);
            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;

        }


        protected void edit(object sender, EventArgs e)
        {

            string title = pageTitle.Text.ToString();
            string content = Content.Text.ToString();
            string query = "Update pages set pagetitle = '" + title + "', pagecontent = '" + content + "' where pageid=" + pageid;
            editpage.UpdateCommand = query;
            editpage.Update();
            result.InnerHtml = "Page is Edited";

        }


        protected void delete(object sender, EventArgs e)
        {
            string query = "DELETE FROM pages WHERE pageid=" + pageid;
            delpage.DeleteCommand = query;
            delpage.Delete();
            result.InnerHtml = "Page is Deleted";


        }



    }
}