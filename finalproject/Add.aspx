﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="finalproject.Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3>New page</h3>
   <asp:SqlDataSource runat="server" id="insert"
        ConnectionString="<%$ ConnectionStrings:school_sql_con %>">

    </asp:SqlDataSource> 
    
        <label>Enter Page title:</label>
       
        <asp:TextBox ID="pageTitle" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"  ControlToValidate="pageTitle"  ErrorMessage="Enter Page Title">
        </asp:RequiredFieldValidator>
          
      <div>     
     <label>Enter Page content:</label></div>
    <div>
     <asp:TextBox ID="Content" TextMode="multiline" Columns="20" Rows="10"  runat="server" ></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="Content" ErrorMessage="Enter Page Content">
        </asp:RequiredFieldValidator>
   </div>
      <div>
    <ASP:Button id="addpage" Text="Add Page" runat="server" OnClick="add_Page" />      
    </div>
    <div id="result" runat="server"></div>

</asp:Content>
